
require 'trollop'
require 'singleton'
require 'thread'

require 'facter'

require 'agent_charlie/command_line'
require 'agent_charlie/manifest_collection'
require 'agent_charlie/progress'
require 'agent_charlie/event'

class AgentCharlie::Charlie
    include Singleton

    attr_reader :manifest_collection
    attr_reader :ret_code
    attr_accessor :ui

    attr_reader :current_action
    
    attr_reader :event

    def initialize
        @manifest_collection = AgentCharlie::ManifestCollection.new(File.join(AgentCharlie.charlie_root, "manifests"), "pp")
        @manifest_collection.process_manifests

        @action_queue = Queue.new

        @current_action = nil
        @should_run_mutex = Mutex.new
        @should_run = true

        @ret_code = 1
        @ui = nil

        @event = AgentCharlie::Event.new
    end

    def halt_charlie(ret_code)
        AgentCharlie::Logger.debug{"Halt request received"}
        @should_run_mutex.synchronize do
            @should_run = false
        end
        @ret_code = ret_code

        AgentCharlie::Logger.debug{"Halt request processed (ret_code = '#{@ret_code}')"}
    end

    def should_run
        res = false
        @should_run_mutex.synchronize do
            res = @should_run
        end
        return res
    end

    # Starts up Agent Charlie on a thread and waits for events to come in
    def run_threaded
        thr = Thread.new {

            AgentCharlie::Logger.debug{"Starting Charlie thread"}

            while should_run || ! @action_queue.empty?
                begin
                    @current_action, progress = @action_queue.pop(true)
                rescue
                    # Nothing in the queue :(
                    sleep 0.1
                    next
                end

                if @current_action != nil
                    AgentCharlie::Logger.debug{"Popped action '#{@current_action.class.to_s}' off queue"}

                    progress.begin("Starting action '#{@current_action.class.to_s}'")
                    @current_action.run_action(progress)
                else
                    AgentCharlie::Logger.debug{"Popped nil action off queue - (should_run = #{@should_run})"}
                end
            end

        }

        return thr
    end

    # adds an action to the queue, and an associated block to call whenever the
    # action updates. The block is passed a progress object which indicates the stage
    # that the action is in (simple progress tracking)
    # Will call the block with the progress object
    def add_action(action, &block)
        progress = AgentCharlie::Progress.new(&block)

        AgentCharlie::Logger.debug{"Action '#{action.class.to_s}' added to queue"}
        @action_queue.push([action, progress])

        return progress
    end

end
