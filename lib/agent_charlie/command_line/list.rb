
require 'agent_charlie/action'
require 'trollop'

class AgentCharlie::CommandLine::List < AgentCharlie::CommandLine::Base
    def initialize
        super("list", "Lists")
    end

    def execute(args)
        charlie = AgentCharlie::Charlie.instance

        subcommand_opts = Trollop::options(args) do
            banner <<-EOS.gsub(/^\t*/, '')
				Lists all available manifests

				Usage:
				   #{$0} ... list [options]

				Options:
				EOS
        end

        # Sort the manifests
        charlie.add_action(AgentCharlie::Action::List.new(args)) do |progress|
            if progress.stage == :FINISHED
                # List all the manifests
                puts AgentCharlie::CommandLine.generate_doc(progress.result)
            elsif progress.stage == :FAILED
                AgentCharlie::Logger.error{progress.message}
            elsif progress.stage == :RUNNING
                AgentCharlie::Logger.debug{"#{progress.percentage}% - #{progress.message}"}
            end
        end
    end
end

AgentCharlie::CommandLine.newCommand(AgentCharlie::CommandLine::List)