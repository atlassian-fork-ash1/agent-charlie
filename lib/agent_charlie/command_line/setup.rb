require 'agent_charlie/action'

class AgentCharlie::CommandLine::Setup < AgentCharlie::CommandLine::Base
    def initialize
        super('setup', 'Initial setup of your environment')
    end

    def execute(args)
        charlie = AgentCharlie::Charlie.instance

        subcommand_opts = Trollop::options(args) do
            banner <<-EOS.gsub(/^\t*/, '')
				The setup command sets up everything

				Usage:
				   #{$0} ... setup [options]

				Options:
				EOS
            opt :reinstall, 'Marks the specified manifest for re-installation', :short => '-r', :default => false
        end

        if args.length != 0
            Trollop::die "Unknown additional arguments: #{args.inspect}"
        end

        AgentCharlie::CommandLine::Setup.tsort_setup(nil, subcommand_opts)
    end

    def self.tsort_setup(arg, opts)
        charlie = AgentCharlie::Charlie.instance

        # Sort the manifests
        charlie.add_action(AgentCharlie::Action::TSort.new(arg)) do |progress|
            # Apply the manifests if there was no problem
            if progress.stage == :FINISHED
                manifests = progress.result
                should_proceed = true

                # Ask the user if they want to continue
                puts AgentCharlie::CommandLine.generate_doc(manifests)
                should_proceed = charlie.event.raise_event_single(AgentCharlie::Event::BooleanInfoRequest.new("Do you want to continue? ", true) do
                    true
                end)

                if should_proceed
                    do_setup(manifests, opts[:reinstall])
                else
                    AgentCharlie::Logger.debug{'User cancelled setup'}
                end
            elsif progress.stage == :FAILED
                AgentCharlie::Logger.error{"Failed to sort dependencies - #{progress.message}"}
            end
        end
    end

    def self.do_setup(manifests, reinstall)
        charlie = AgentCharlie::Charlie.instance

        charlie.add_action(AgentCharlie::Action::Apply.new({
            :reinstall => reinstall,
            :manifests  => manifests
        })) do |progress|
            if progress.stage == :FINISHED
                AgentCharlie::Logger.info{'Installation finished'}
            elsif progress.stage == :FAILED
                AgentCharlie::Logger.error{"Could not finish installation - #{progress.message}"}
            elsif progress.stage == :RUNNING
                AgentCharlie::Logger.info{"#{progress.percentage}% - #{progress.message}"}
            end
        end
    end
end

AgentCharlie::CommandLine.newCommand(AgentCharlie::CommandLine::Setup)
