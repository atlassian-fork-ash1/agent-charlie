
class AgentCharlie::Event

    @@events = []

    def self.register_event(name, cls)
        AgentCharlie::Logger.debug{"Registering event #{name}"}
        @@events << cls
    end

    def initialize
        # Create a hash of event class to list
        @listeners = {}
        @@events.each do |cls|
            @listeners[cls] = []
        end
    end

    def register_listener(cls, &block)
        unless @@events.include?(cls)
            raise 'Listener registration failed'
        end

        # Register the block
        @listeners[cls] << block
    end

    def get_listeners(event)
        cls = event.class
        until ! cls || @listeners[cls].length > 0
            cls = cls.superclass
        end

        return [] unless cls
        return @listeners[cls]
    end

    # Raise an event and return stuff
    def raise_event(event)
        result = []

        # Call the listeners
        get_listeners(event).each do |listener|
            listener_result = listener.call(event)

            event.verify_result(listener_result) or raise 'Listener returned an invalid result'

            result << listener_result
        end

        result
    end

    # THERE CAN BE ONLY ONE
    def raise_event_single(event)
        raise 'THERE CAN BE ONLY ONE' if get_listeners(event).length != 1

        result = get_listeners(event)[0].call(event)
        event.verify_result(result) or raise 'Listener returned an invalid result'

        result
    end

end

# Include all event modules
Dir[File.join(File.expand_path('lib/agent_charlie/event', AgentCharlie.charlie_root), '*.rb')].each do |file|
    require 'agent_charlie/event/' + File.basename(file, File.extname(file))
end