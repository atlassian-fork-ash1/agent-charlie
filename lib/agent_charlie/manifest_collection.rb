
require 'agent_charlie/manifest'
require 'agent_charlie/logger'

require 'yaml'
require 'tsort'

class AgentCharlie::ManifestCollection

    include TSort

    def initialize(manifest_dir, manifest_ext)
        @manifest_dir = manifest_dir
        @manifest_ext = manifest_ext

        @manifest_hash = {}
    end

    def tsort_each_node(&block)
        @manifest_hash.each_key(&block)
    end

    def tsort_each_child(node, &block)
        @manifest_hash[node].dependencies.each(&block)
    end

    def get_manifest(name)
        @manifest_hash[name]
    end

    def get_manifests
        @manifest_hash.values
    end

    def process_manifests
        manifest_files = Dir[File.join(File.expand_path(@manifest_dir), "*.#{@manifest_ext}")]

        manifest_files.each do |manifest|
            manifest_key = File.basename(manifest, ".#{@manifest_ext}")
            AgentCharlie::Logger.debug{"Discovered manifest '#{manifest_key}'"}

            # Open the file and reading
            File.open(manifest) do |file|
                # Extract all the lines that start with "#" at the start of the file
                metadata_lines = file.each_line.take_while do |line|
                    line.start_with?('# ')
                end.map do |filtered|
                    filtered[/^# (.*)/, 1]
                end

                # Use a YAML parser to parse the rest of this stuff
                metadata = YAML.load(metadata_lines.join("\n"))

                # Put this data into the manfiest
                # Initialize each manifest
                manifest_obj = AgentCharlie::Manifest.new(manifest_key)
                if manifest_obj.process_metadata(metadata)
                    @manifest_hash[manifest_key] = manifest_obj
                end
            end
        end

        # Verify all manifests in everything are ok
        AgentCharlie::Logger.debug{'Verifying all manifests are valid'}
        @manifest_hash.each_value do |man|
            deps = man.dependencies
            if (@manifest_hash.keys & deps).sort != deps.sort
                AgentCharlie::Logger.error{"Invalid dependencies detected in manifest #{man.key}"}
                abort
            end
        end

        # Perform cycle detection
        AgentCharlie::Logger.debug{'Performing cycle detection'}
        each_strongly_connected_component do |ns|
            if ns.length != 1
                fs = ns.delete_if {|n| Array === n}
                AgentCharlie::Logger.error{"Cyclic dependencies: #{fs.join ', '}"}
                abort
            end
        end
    end

    def get_tsorted_manifests
        tsorted = tsort
        AgentCharlie::Logger.debug{"Returning sorted manifests: '#{tsorted.inspect}'"}
        tsorted
    end

    def get_specific_tsorted_manifests(manifest_name)
        # Need to manually do this
        arr = []
        each_strongly_connected_component_from(manifest_name) do |node|
            arr << node.first
        end
        AgentCharlie::Logger.debug{"Returning sorted manifests: '#{arr.inspect}'"}
        arr
    end

end