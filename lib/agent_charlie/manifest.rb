
require 'agent_charlie/install_method'

class AgentCharlie::Manifest

    attr_reader :key
    attr_reader :name
    attr_reader :description
    attr_reader :dependencies
    attr_reader :method

    def initialize(key)
        @key = key
    end

    def process_metadata(metadata)
        @name = metadata['name'] || @key
        @description = metadata['description'] || ''

        # Fun fun
        begin
            operating_system_specific = metadata['operatingsystems'][AgentCharlie::operating_system_str]
        rescue
            AgentCharlie::Logger.error{"Syntax error in manifest '#{@key}': 'operatingsystems' not found"}
            return false
        end

        begin
            AgentCharlie::Logger.debug{"Manifest '#{@key}' not needed for Operating System '#{AgentCharlie::operating_system_str}'. Skipping."}
            return false
        end unless operating_system_specific

        begin
            install_method = operating_system_specific['install-method']['method']
        rescue
            AgentCharlie::Logger.error{"Syntax error in manifest '#{@key}': Installation method was not provided"}
            return false
        end

        begin
            @method = AgentCharlie::InstallMethod.get_method(install_method, metadata['operatingsystems'][AgentCharlie::operating_system_str]['install-method'])
        rescue
            AgentCharlie::Logger.error{"Syntax error in manifest '#{@key}': Error parsing method parameters"}
            return false
        end

        @dependencies = operating_system_specific['dependencies'] || []

        true
    end

end