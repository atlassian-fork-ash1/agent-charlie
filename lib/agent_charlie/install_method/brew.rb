class AgentCharlie::InstallMethod::Brew < AgentCharlie::InstallMethod::Base

    def self.description
        "Installed using Homebrew"
    end

    def parse(data)
        @package = data["brew-package"] || "?"
    end

    def description
        return "Using package '#{@package}'"
    end

end

AgentCharlie::InstallMethod.new_method(AgentCharlie::InstallMethod::Brew, "brew")