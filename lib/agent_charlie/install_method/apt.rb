class AgentCharlie::InstallMethod::Apt < AgentCharlie::InstallMethod::Base

    def self.description
        "Installed using apt-get"
    end

    def parse(data)
        @repository = data["apt-repository"] || "?"
        @package    = data["apt-package"] || "?"
    end

    def description
        return "Using repository '#{@repository}' and package '#{@package}'"
    end

end

AgentCharlie::InstallMethod.new_method(AgentCharlie::InstallMethod::Apt, "apt")