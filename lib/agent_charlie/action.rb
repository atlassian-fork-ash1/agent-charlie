
class AgentCharlie::Action
    def run_action(progress)
        raise "Action not implemented"
    end

    def get_progress
        raise "Gets a progress"
    end
end

Dir[File.join(File.expand_path('lib/agent_charlie/actions', AgentCharlie.charlie_root), '*.rb')].each do |file|
    require 'agent_charlie/actions/' + File.basename(file, File.extname(file))
end