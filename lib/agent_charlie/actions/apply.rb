
require 'puppet/util/command_line'

class AgentCharlie::Action::Apply

    def initialize(params = {})
        @reinstall = params[:reinstall] || false
        @manifests = params[:manifests]

        @progress = nil

        AgentCharlie::Logger.debug{"Apply action created"}
    end

    def run_action(progress)
        @progress = progress

        manifest_keys = @manifests.map{ |m| m.key }

        # Run puppet setup
        wd = Dir.getwd
        Dir.chdir(AgentCharlie.charlie_root)

        custom_args = [
            '--libdir', 'lib',
            '--modulepath', 'modules',
            '--manifestdir', 'manifests',
            '--factpath', 'lib/facts',
        ]
        custom_args += manifest_keys

        # Launch puppet
        Puppet::Util::CommandLine.new('compile', custom_args).execute

        Dir.chdir(wd)
    end

    def get_progress
        return @progress
    end

end
