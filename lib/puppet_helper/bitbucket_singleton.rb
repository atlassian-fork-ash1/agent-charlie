require 'puppet_helper/generic_login_singleton'

require 'rest-client'

if not Object.const_defined?('BitbucketSingleton')
    class BitbucketSingleton < GenericLoginSingleton

        def initialize
            super('Bitbucket', 'Username or email')
        end

        def authenticate
            begin
                RestClient::Request.execute(
                    :method     =>  :get,
                    :url        =>  'https://bitbucket.org/api/1.0/user',
                    :user       =>  @username,
                    :password   =>  @password
                )

                return true
            rescue
                return false
            end
        end

        def information
            puts <<-'EOS'.gsub(/^\t*/, '')

			Agent Charlie requires your Bitbucket account details so we can install your private key and check out some
			useful tools to set up your development environment.

            EOS
        end

    end
end
