
if not Object.const_defined?('CharlieKeyring')
    class CharlieKeyring
        def initialize(displayName, name)
            raise "Override me"
        end

        def storePassword(username, password)
            raise "Override me"
        end

        def refreshFromKeyring()
            raise "Override me"
        end

        def retrieveUsername()
            raise "Override me"
        end

        def retrievePassword()
            raise "Override me"
        end
    end
end