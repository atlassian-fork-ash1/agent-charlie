require 'puppet_helper/generic_login_singleton'

require 'rest-client'

if not Object.const_defined?('StashSingleton')
    class StashSingleton < GenericLoginSingleton

        def initialize
            super('Stash', 'Username')
        end

        def authenticate
            begin
                RestClient::Request.execute(
                    :method     =>  :get,
                    :url        =>  'https://stash.atlassian.com/rest/api/1.0/projects',
                    :user       =>  @username,
                    :password   =>  @password
                )

                return true
            rescue
                return false
            end
        end

        def information
            puts <<-'EOS'.gsub(/^\t*/, '')

			Agent Charlie requires your Stash account details so we can install your private key and check out some
			useful tools to set up your development environment.

            EOS
        end

    end
end
