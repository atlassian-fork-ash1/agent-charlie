# name: Git
# description: Git, the smart content tracker
# operatingsystems:
#   Darwin:
#     install-method:
#       method:         brew
#       brew-package:   git
#     dependencies:
#       - homebrew
#   Ubuntu:
#     install-method:
#       method:         apt
#       apt-repository: canonical
#       apt-package:    git
#     dependencies: []

include git
