# name: Homebrew
# description: OS X Package Manager
# operatingsystems:
#   Darwin:
#     install-method:
#       method:         manual
#       manual-desc:    Installed via official homebrew installer
#     dependencies:
#       - xcode

if $::operatingsystem == 'Darwin' {
    class {'homebrew': user => $::config_user }
} else {
    notify {'Skipping Homebrew - not OSX!':}
}
