# name: Java Development Kit 7
# description: Latest JDK
# operatingsystems:
#   Darwin:
#     install-method:
#       method:         pkgdmg
#     dependencies: []
#   Ubuntu:
#     install-method:
#       method:         apt
#       apt-repository: ppa:webupd8team/java
#       apt-package:    oracle-java7-installer
#     dependencies: []

include jdk