# name: XCode Command Line Tools
# description: Apple XCode Command Line Tools
# operatingsystems:
#   Darwin:
#     install-method:
#       method:         pkgdmg
#     dependencies: []

if $::operatingsystem == 'Darwin' {
    include xcode
} else {
    notify {'Skipping XCode - not OSX!':}
}
