# name: Subversion
# description: Very old VCS
# operatingsystems:
#   Darwin:
#     install-method:
#       method:         brew
#       brew-package:   subversion
#     dependencies:
#       - homebrew
#   Ubuntu:
#     install-method:
#       method:         apt
#       apt-repository: canonical
#       apt-package:    subversion
#     dependencies: []


include svn
