# name: Atlassian SDK
# description: The Atlassian Software Development Kit
# operatingsystems:
#   Darwin:
#     install-method:
#       method:         brew
#       brew-package:   atlassian/tap/atlassian-plugin-sdk
#     dependencies:
#       - homebrew
#   Ubuntu:
#     install-method:
#       method:         apt
#       apt-repository: https://sdkrepo.atlassian.com/debian/
#       apt-package:    atlassian-plugin-sdk
#     dependencies: []


include atlas-sdk
