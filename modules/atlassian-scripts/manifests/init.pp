class atlassian-scripts {
    $tmp = $::config_tmp_directory
    $target = $::config_atlassian_home
    $user_home = $::config_home
    $user = $::config_user
    $group = $::config_group

    # We have an EAC test user now, so we can do this
    exec {'get_atlassian_scripts':
        command =>  "git clone git@bitbucket.org:atlassian/atlassian-scripts.git '${target}/atlassian-scripts'",
        creates =>  "${target}/atlassian-scripts",
        user    =>  "$user",
        path    =>  ['/usr/bin', '/bin'],
    }

    file {'atlassian_scripts_ownership':
        path    => "${target}/atlassian-scripts",
        owner   => $user,
        group   => $group,
        recurse => inf,
        require => Exec['get_atlassian_scripts'],
    }

    # Set up the environment for atlassian-scripts
    file{'atlassian_scripts_environment':
        ensure  =>  present,
        path    =>  "${target}/env/atlassian-scripts-environment",
        owner   =>  $user,
        group   =>  $group,
        content =>  template('atlassian-scripts/atlassian-scripts-environment.erb'),
        recurse =>  inf,
        require =>  Exec['get_atlassian_scripts'],
    }
}
