class stash-keys {
    $stash_username = stash_username()
    $stash_password = stash_password()

    $user_home = $::config_home
    $user = $::config_user
    $group = $::config_group
    $charlie_dir = $::config_charlie_home

    # Upload your stash keys
    upload_stash_keys{'upload_stash_keys':
        home           => $user_home,
        stash_username => $stash_username,
        stash_password => $stash_password
    }

}