# == Class: jdk::ubuntu
#
# Sets up the JDK for Ubuntu
#

class jdk::ubuntu {
    $user = $::config_user
    $group = $::config_group
    $target = $::config_atlassian_home

    # Create repositories and stuff
    exec {'update_apt':
        command => '/usr/bin/apt-get update'
    }

    exec {'jdk_repo':
        command => '/usr/bin/add-apt-repository -y ppa:webupd8team/java',
        notify  => Exec['update_apt'],
        creates => '/etc/apt/sources.list.d/webupd8team-java.list',
    }

    # Oracle automatically accept license
    exec {'go_away_oracle':
        command =>
            '/bin/echo oracle-java7-installer \
                shared/accepted-oracle-license-v1-1 \
                select true | /usr/bin/debconf-set-selections',
        unless  => '/usr/bin/dpkg -l | grep oracle-java7-installer'
    }

    # Install Oracle Java
    package{'oracle-java7-installer':
        ensure  => 'installed',
        require => [Exec['jdk_repo'], Exec['go_away_oracle'], Exec['update_apt']],
    }

    # JDK environment file
    file{'jdk_environment':
        ensure  =>  present,
        path    =>  "${target}/env/jdk-environment",
        owner   =>  $user,
        group   =>  $group,
        content =>  'export JAVA_HOME=`readlink -f /usr/bin/javac | sed "s:bin/javac::"`',
        recurse =>  inf,
    }
}
