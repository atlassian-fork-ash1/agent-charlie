require 'highline/import'

Puppet::Parser::Functions.newfunction(:read_ssh_password, :arity => 0,
    :type => :rvalue) do |args|
	# maven_password_file = File.expand_path('~/maven_master_password')
	# if File.exist?(maven_password_file)
	# 	pw = File.open(maven_password_file, &:readline)
	# end

    pw = nil
    begin
        pw = Facter.config_ssh_keypass || ""
    rescue Exception => e        
    end

	if pw == nil
        charlie = AgentCharlie::Charlie.instance
		HighLine.track_eof=(false)

        confirm_pw = nil

        while confirm_pw == nil
            begin
                pw = charlie.event.raise_event_single(AgentCharlie::Event::PasswordInfoRequest.new("Create an SSH passphrase (or empty for none): ") do |input|
                    true
                end)

                if pw.length != 0 && pw.length < 4
                    say("SSH passphrase too short - must be 4 letters or longer")
                end
            end while pw.length != 0 && pw.length < 4

            confirm_pw = charlie.event.raise_event_single(AgentCharlie::Event::PasswordInfoRequest.new("Confirm SSH passphrase: ") do |input|
                true
            end)

            if pw != confirm_pw
                pw = nil
                confirm_pw = nil
                say("Passwords do not match")
            end
        end
    end

    pw.chomp

end
