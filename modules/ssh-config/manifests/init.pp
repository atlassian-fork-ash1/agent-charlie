class ssh-config {
    $user = $::config_user
    $group = $::config_group
    $user_home = $::config_home

    file {'ssh_directory':
        ensure  =>  directory,
        path    =>  "${user_home}/.ssh",
        owner   =>  $user,
        group   =>  $group,
        mode    =>  '0700',
    }

    file {'ssh_config':
        path    =>  "${user_home}/.ssh/config",
        owner   =>  $user,
        group   =>  $group,
        mode    =>  '0744',
        content =>  template('ssh-config/config.erb'),
        require =>  File['ssh_directory'],
        replace =>  false,
    }

    file {'known_hosts':
        path    =>  "${user_home}/.ssh/known_hosts",
        owner   =>  $user,
        group   =>  $group,
        mode    =>  '0744',
        content =>  template('ssh-config/known_hosts.erb'),
        require =>  File['ssh_directory'],
        replace =>  false,
    }
}
