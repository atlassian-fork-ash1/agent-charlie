
require 'rest-client'
require 'json'

Puppet::Type.newtype(:upload_bitbucket_keys) do
    @doc = "Uploads your private key to Bitbucket"

    newproperty(:stupid_hack) do |property|
        desc "Stupid hack to get puppet to do what we want"

        defaultto :insync

        def retrieve
            :outofsync
        end

        def sync
            sshkey = File.read("#{self.resource[:home]}/.ssh/id_rsa.pub").chomp

            # Grab the *real* username
            response = RestClient::Request.execute(
                :method     =>  :get,
                :url        =>  "https://bitbucket.org/api/1.0/user",
                :user       =>  self.resource[:username],
                :password   =>  self.resource[:password]
                )
            from_json = JSON.parse(response)
            real_username = from_json['user']['username']

            # Send the SSH key
            begin
                RestClient::Request.execute(
                    :method     =>  :post,
                    :url        =>  "https://bitbucket.org/api/1.0/users/#{real_username}/ssh-keys",
                    :user       =>  real_username,
                    :password   =>  self.resource[:password],
                    :payload    =>  {
                        :label  =>  'charlie-installed-key',
                        :key    =>  sshkey
                    }
                )
            rescue RestClient::BadRequest
                # Ignore this because it signals a duplicate key
            rescue => e
                self.fail "Could upload SSH keys: #{e.message}"
            end

            :insync
        end
    end

    newparam(:name) do
        desc "The name of the resource"
    end

    newparam(:home) do
        desc "The home directory of the user"
    end

    newparam(:username) do
        desc "Your bitbucket username"
    end

    newparam(:password) do
        desc "Your bitbucket password"
    end

end
