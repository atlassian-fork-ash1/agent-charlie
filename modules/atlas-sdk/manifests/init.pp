class atlas-sdk {
    $tmp = $::config_tmp_directory
    $charlie_dir = $::config_charlie_home

    $user = $::config_user
    $group = $::config_group

    case $::operatingsystem {
        ubuntu: {
            exec {'atlassian-repository':
                command => 'echo "deb https://sdkrepo.atlassian.com/debian/ stable contrib" >>/etc/apt/sources.list',
                path    => ['/usr/local/bin','/usr/local/sbin','/usr/bin','/usr/sbin','/bin','/sbin'],
                unless  => 'grep -Ee "^deb https://sdkrepo\\.atlassian\\.com/debian/ stable contrib$" /etc/apt/sources.list',
                notify  => Exec['update-apt-repo'],
            }

            exec {'atlassian-public-key':
                command => 'apt-key adv --keyserver keyserver.ubuntu.com --recv-keys B07804338C015B73',
                path    => ['/usr/local/bin','/usr/local/sbin','/usr/bin','/usr/sbin','/bin','/sbin'],
                require => Exec['atlassian-repository'],
            }

            package {'atlassian-plugin-sdk':
                ensure  => installed,
                require => [Exec['atlassian-repository'],Exec['atlassian-public-key'],Exec['update-apt-repo']],
            }

            exec {'update-apt-repo':
                command => 'apt-get update',
                path    => ['/usr/local/bin','/usr/local/sbin','/usr/bin','/usr/sbin','/bin','/sbin'],
            }
        }
        Darwin: {
            exec {'install_atlas_sdk_tap':
                command => "${charlie_dir}/bin/brew tap atlassian/tap",
                unless  => "${charlie_dir}/bin/brew tap | /usr/bin/grep -Ee '^atlassian/tap$'",
            }


            exec {'install_atlas_sdk':
                command => "${charlie_dir}/bin/brew-bottle atlassian/tap/atlassian-plugin-sdk",
                creates => '/usr/local/Cellar/atlassian-plugin-sdk',
                require => Exec['install_atlas_sdk_tap']
            }
        }
        default: {
            fail("Unsupported operating: ${::operatingsystem}")
        }
    }
}
