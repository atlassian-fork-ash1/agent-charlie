#!/bin/bash

# Global variables
declare -a manifestList
declare installEverything=false

declare reinstall=''
declare slow=true
declare bootstrap=true
declare target
declare local_repository="$HOME/repository"
declare testfile_name
declare build
declare password='NotSet'

# Everythings runs from here
function _main() {
    readArguments "$@"
    
    startVM

    run "sudo df -h"

    if [ -f testing.zip ]; then
        rm -rf testing.zip
    fi
    zip -q --exclude "test" --exclude "*osx.vmware*" --exclude "bin/*" --exclude "*Ubuntu*" --exclude "*.bundle*" --exclude "*package.box*" --exclude "*.git*" -r testing.zip .

    # Copy testing files over
    copy testing.zip testing.zip
    copy bootstrap.sh bootstrap.sh
    if $bootstrap; then
        run "./bootstrap.sh -test -overwrite"

        # Copy the crowd username and stuff over
        local targetFile="~/atlassian/agent-charlie/setup/agent_bin/charlie.conf"
        run "echo \"crowd_username : 'automated-dev-test-user'\" >> $targetFile"
        run "echo \"crowd_password : '${password}'\" >> $targetFile"
        run "echo \"maven_master_password : 'some_password'\" >> $targetFile"
        run "echo \"ssh_keypass : ''\" >> $targetFile"
        run "echo \"bitbucket_username : 'automated-dev-test-user'\" >> $targetFile"
        run "echo \"bitbucket_password : '${password}'\" >> $targetFile"
    else
        run "unzip -o -qq -d \"\$HOME/atlassian/agent-charlie\" testing.zip"
    fi

    if [[ "${#manifestList[@]}" -ne 0 ]] || $installEverything; then
        if ! $installEverything; then
            for manifest in "${manifestList[@]}"; do
                run "bash --login -c \"charlie --verbose --unattended install $reinstall $manifest\""
            done
        else
            run "bash --login -c \"charlie --verbose --unattended setup $reinstall\""
        fi
    else
        echo "*** WARNING ***:"
        echo "Nothing was specified for install"
        echo "Charlie was bootstrapped only."
    fi

    if [ -n "$build" ]; then
        echo building: $build
        copy "test/$build.sh" "$build.sh"
        run "bash $build.sh"
    fi
}

function printUsage() {
cat <<-EOF
Usage: $0 [options] (Ubuntu|Darwin)

These are installed in the order they are specified:
   -m|--manifest <manifest>     Selects a manifest to install...
   -e|--everything              ... or just install everything

Additional options:
   -r|--reinstall               Reinstall
   -b|--build <product>         Builds a product
   -h|--help                    Help text
   -s|--skip-bootstrap          Skips bootstrapping (implies --quick)
   -q|--quick                   Skips booting the VM
   -p|--password <password>     Set the password for the automated_dev_test_user
EOF
}

#
# Reads parameters and stuff
#
function readArguments() {
    # Parse arguments
    while [[ "$#" -gt 0 ]]; do
        case "$1" in
            -r|--reinstall)
                reinstall="--reinstall"
            ;;
            -q|--quick)
                slow=false
            ;;
            -m|--manifest)
                manifestList+=("$2")
                shift 1
            ;;
            -b|--build)
                build=$2
                shift 1
            ;;
            -e|--everything)
                installEverything=true
            ;;
            -h|--help)
                printUsage
                exit 1
            ;;
            -s|--skip-bootstrap)
                bootstrap=false
                slow=false
            ;;
            -p|--password)
                password=$2
                shift 1
            ;;
            -*)
                echo "Unknown option: $1"
                printUsage
                exit 1
            ;;
            *)
                target=$1
                shift 1
                break
            ;;
        esac
        shift 1
    done

    if [[ -z "$target" ]]; then
        printUsage
        exit 1
    fi

    if [[ "$#" -gt 0 ]]; then
        echo "Unrecognized additional options: $@"
        exit 1
    fi
}

#
# Defines the run(), copy() and startvm() functions depending on whether
# we are running this script for a Ubuntu or Darwin installation
#
function startVM() {

    chmod 600 test/test_id_rsa

    case "$target" in
        Ubuntu)
            DIR=`pwd`
            NAME="Ubuntu"

            function run() {
                vagrant ssh -c "$1"
            }

            function copy() {
                vagrant ssh-config > $DIR/vagrant-ssh-config
                scp -F "$DIR/vagrant-ssh-config" "$1" vagrant@default:"$2"
            }

            if $slow; then
                vagrant halt
                vagrant destroy -f
                vagrant up
                run "sudo apt-get clean"
                run "sudo apt-get update"
                run "sudo apt-get -y install python-software-properties"
            fi
            run "sudo free -m"
        ;;
        Darwin)
            local DIR=`pwd`
            local VMRUN="/Applications/VMware Fusion.app/Contents/Library/vmrun"
            local VMX="$DIR/osx.vmware/Mac OS X 10.8 64-bit.vmx"

            function run() {
                ssh -i test/test_id_rsa atlassian@${vmAddress} "$1"
            }

            function copy() {
                scp -i test/test_id_rsa "$1" atlassian@${vmAddress}:"$2"
            }

            local nogui=""
            if [ "`hostname`" = "agent-charlie.syd.atlassian.com" ]; then
                echo "Running on bamboo, setting nogui and overriding vmx location"
                nogui="nogui"
                VMX="/Users/atlassian/Documents/Virtual Machines.localized/osx.vmware/osx.vmx"
            fi

            if $slow; then
                "$VMRUN" revertToSnapshot "$VMX" Base
                "$VMRUN" -T fusion start "$VMX" ${nogui}
                "$VMRUN" unpause "$VMX"
            fi

            # wait for guest to start up
            for run in {1..10}
            do
                vmAddress=`"$VMRUN" readVariable "$VMX" guestVar ip`
                ssh -i test/test_id_rsa atlassian@${vmAddress} "ls"
                if [ "$?" = "0" ]; then
                    echo "Connection established at ${vmAddress}"
                    break
                else
                    echo "Waiting for guest to start..."
                    sleep 3;
                fi
            done
        ;;
        *)
            echo unsupported platform: "$target"
            exit 1
        ;;
    esac
}

_main "$@"
