#!/bin/bash

echo Building JIRA...
if [ -d $HOME/src/jira ]; then
    rm -rf $HOME/src/jira
fi

mkdir -p $HOME/.m2/repository
cd $HOME/.m2/repository
curl -L http://agent-charlie.syd.atlassian.com/repository/jira-repository.tar.bz2 | bzcat | tar -xf -

. ~/.profile
mkdir -p $HOME/src
cd $HOME/src
atlassian-checkout jira
cd jira
mvn install -DskipTests
