require 'spec_helper'

describe 'maven::settings' do

    let(:facts) {
    {
        'config_user' => "adam",
        'config_home' => "/user/adam",
        'config_group' => "jensen",
        'config_atlassian_home' => "/hold/horses",
        'config_tmp_directory' => "/hold/horses/tmp",
        'config_charlie_home' => "/hold/horses/agent-charlie",
        'charlie_repository' => "http://www.charlie-repository.com/",
        'config_crowd_username' => 'some_crowd_username',
        'config_crowd_password' => 'some_crowd_password',
        'config_maven_master_password' => 'some_master_password',
        'operatingsystem' => 'Ubuntu',
        'operatingsystemrelease' => '12.04',
    }}

    let(:title) { 'mavensettings_3.0.4' }
    let(:params) {{
        'version'=>'3.0.4',
    }}

    it {

        should contain_exec('tmp_mvn_password').with({
            :command => "sudo -u 'adam' '/hold/horses/bin/mvn3.0.4' --encrypt-password 'some_crowd_password' 1> '/user/adam/.m2/tmp_password'",
            :creates => '/user/adam/.m2/tmp_password'
        })

        should contain_exec('sed_settings_xml_replace').with({
            :command => "sed -E -e 's\#yourusername\#some_crowd_username\#g;'\"s\#yourpassword\#$(cat '/user/adam/.m2/tmp_password')\#g\" '/user/adam/.m2/settings.xml.developer' > '/user/adam/.m2/settings.xml'",
        })
    }

end
