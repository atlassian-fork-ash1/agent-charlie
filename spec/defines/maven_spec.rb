require 'spec_helper'

describe 'maven' do
    default_facts = {
        'config_user' => "adam",
        'config_home' => "/user/adam",
        'config_group' => "jensen",
        'config_atlassian_home' => "/hold/horses",
        'config_tmp_directory' => "/hold/horses/tmp",
        'config_charlie_home' => "/hold/horses/agent-charlie",
        'charlie_repository' => "http://www.charlie-repository.com",
        'config_maven_master_password' => 'some_maven_master_password',
        'config_crowd_username' => 'some_crowd_user',
        'config_crowd_password' => 'some_crowd_password',
    }

    context 'with maven 2.1.0' do
        let(:title) { 'maven_2.1.0' }
        let(:params) { {'version'=>'2.1.0'} }

        # rspec-puppet doesn't stub custom facts?
        let(:facts) {default_facts.merge({
            :operatingsystem        => 'Ubuntu',
            :operatingsystemrelease => 12.04,
        })}

        it do
            should contain_exec('maven_get_2.1.0').with({
                :command => '/usr/bin/curl -L -s -o \'/hold/horses/tmp/apache-maven-2.1.0-bin.zip\' \'http://www.charlie-repository.com/apache-maven-2.1.0-bin.zip\'',
                :creates => '/hold/horses/tmp/apache-maven-2.1.0-bin.zip'
            })

            should contain_exec('maven_unzip_2.1.0').with({
                :command => "/usr/bin/unzip -nd '/hold/horses' '/hold/horses/tmp/apache-maven-2.1.0-bin.zip'",
            })

            should contain_file('maven_dir_2.1.0').with({
                :path    => "/hold/horses/apache-maven-2.1.0",
                :owner   => "adam",
                :group   => "jensen",
                :ensure  => "directory",
                :mode    => "644"
            })

            should contain_file('maven_dir_exec_2.1.0').with({
                :path    => "/hold/horses/apache-maven-2.1.0/bin/mvn",
                :owner   => "adam",
                :group   => "jensen",
                :mode    => "755"
            })
        end

        it do
            should contain_file('maven_agent_executable').with({
                :path       =>  '/hold/horses/bin/mvn',
                :owner      =>  'adam',
                :group      =>  'jensen',
                :mode       =>  '755',
            })

            should contain_file('maven_agent_executable2.1.0').with({
                :path       =>  '/hold/horses/bin/mvn2.1.0',
                :owner      =>  'adam',
                :group      =>  'jensen',
                :mode       =>  '755',
            })

            should contain_file('maven_environment_2.1.0').with({
                :path       =>  '/hold/horses/env/mvn2.1.0-environment',
                :owner      =>  'adam',
                :group      =>  'jensen',
            })

        end
    end

    context 'with maven 3.0.5' do
        let(:title) { 'maven_3.0.5' }
        let(:params) { {'version'=>'3.0.5'} }

        # rspec-puppet doesn't stub custom facts?
        let(:facts) {default_facts.merge({
            :operatingsystem        => 'Ubuntu',
            :operatingsystemrelease => 12.04,
            :config_crowd_username         => 'crowd_user',
            :config_crowd_password         => 'crowd_password',
        })}

        it do
            should contain_exec('maven_get_3.0.5').with({
                :command => '/usr/bin/curl -L -s -o \'/hold/horses/tmp/apache-maven-3.0.5-bin.zip\' \'http://www.charlie-repository.com/apache-maven-3.0.5-bin.zip\'',
                :creates => '/hold/horses/tmp/apache-maven-3.0.5-bin.zip'
            })

            should contain_exec('maven_unzip_3.0.5').with({
                :command => "/usr/bin/unzip -nd '/hold/horses' '/hold/horses/tmp/apache-maven-3.0.5-bin.zip'",
            })

            should contain_file('maven_dir_3.0.5').with({
                :path    => "/hold/horses/apache-maven-3.0.5",
                :owner   => "adam",
                :group   => "jensen",
                :ensure  => "directory",
                :mode    => "644"
            })

            should contain_file('maven_dir_exec_3.0.5').with({
                :path    => "/hold/horses/apache-maven-3.0.5/bin/mvn",
                :owner   => "adam",
                :group   => "jensen",
                :mode    => "755"
            })
        end

        it do
            should contain_file('maven_agent_executable3.0.5').with({
                :path       =>  '/hold/horses/bin/mvn3.0.5',
                :owner      =>  'adam',
                :group      =>  'jensen',
                :mode       =>  '755',
            })

            should contain_file('maven_environment_3.0.5').with({
                :path       =>  '/hold/horses/env/mvn3.0.5-environment',
                :owner      =>  'adam',
                :group      =>  'jensen',
            })

        end
    end
end

