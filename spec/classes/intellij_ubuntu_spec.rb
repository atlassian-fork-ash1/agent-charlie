require 'spec_helper'

describe 'idea::intellij::ubuntu' do
    let(:title) { 'intellij_spec' }
    default_facts ={
        :charlie_repository => 'http://www.test-repository.example.com',
        :config_atlassian_home => '/home/ponies/target',
        :config_user => 'bob',
        :config_group => 'bobgroup',
        :config_home => '/home/ponies',
		:config_tmp_directory => '/home/ponies/atlassian/agent-charlie/tmp',
        :config_charlie_home => '/home/charlie/directory',
        :architecture => 'x86_64'
    }
    let(:facts) {default_facts}

    it "Sets up IntelliJ" do
        should contain_file('intellij_directory').with({
            'ensure'  =>  'directory',
            'path'    =>  '/home/ponies/target/intellij12',
            'owner'   =>  'bob',
            'group'   =>  'bobgroup',
        })

        should contain_exec('intellij_get').with({
            'command' =>  "/usr/bin/curl -L -s -o '/home/ponies/atlassian/agent-charlie/tmp/ideaIU12_latest.tar.gz' 'http://www.test-repository.example.com/ideaIU12_latest.tar.gz'",
            'creates' =>  '/home/ponies/atlassian/agent-charlie/tmp/ideaIU12_latest.tar.gz',
        })

        should contain_file('intellij_ownership').with({
            'path'    =>  '/home/ponies/atlassian/agent-charlie/tmp/ideaIU12_latest.tar.gz',
            'owner'   =>  'bob',
            'group'   =>  'bobgroup',
        })

        should contain_exec('intellij_extract').with({
            'cwd'     =>  '/home/ponies/target/intellij12',
            'creates' =>  '/home/ponies/target/intellij12/bin',
        })
    end

    context 'with architecture => 64 bit' do
        let(:facts) {default_facts.merge({
            :architecture => 'x86_64'
        })}

        it "Configures IntelliJ" do
            should contain_exec('intellij_license').with({
                'command' =>  "/usr/bin/curl -L -s -o '/home/ponies/.IntelliJIdea12/config/idea12.key' 'http://www.test-repository.example.com/idea12.key'",
                'creates' =>  '/home/ponies/.IntelliJIdea12/config/idea12.key',
            })

            should contain_file('intellij_license_ownership').with({
                'path'    =>  '/home/ponies/.IntelliJIdea12/config/idea12.key',
                'owner'   =>  'bob',
                'group'   =>  'bobgroup',
            })

            should contain_file('intellij_config').with({
                'ensure'  =>  'present',
                'path'    =>  '/home/ponies/target/intellij12/bin/idea64.vmoptions',
                'owner'   =>  'bob',
                'group'   =>  'bobgroup',
            })

            # Test that the intellij config file has content
            content = catalogue.resource('file', 'intellij_config').send(:parameters)[:content]
            content.should_not be_empty
        end
    end

    context 'with architecture => 32 bit' do
        let(:facts) {default_facts.merge({
            :architecture => 'x86'
        })}

        it "Configures IntelliJ" do
            should contain_exec('intellij_license').with({
                'command' =>  "/usr/bin/curl -L -s -o '/home/ponies/.IntelliJIdea12/config/idea12.key' 'http://www.test-repository.example.com/idea12.key'",
                'creates' =>  '/home/ponies/.IntelliJIdea12/config/idea12.key',
            })

            should contain_file('intellij_license_ownership').with({
                'path'    =>  '/home/ponies/.IntelliJIdea12/config/idea12.key',
                'owner'   =>  'bob',
                'group'   =>  'bobgroup',
            })

            should contain_file('intellij_config').with({
                'ensure'  =>  'present',
                'path'    =>  '/home/ponies/target/intellij12/bin/idea.vmoptions',
                'owner'   =>  'bob',
                'group'   =>  'bobgroup',
            })

            # Test that the intellij config file has content
            content = catalogue.resource('file', 'intellij_config').send(:parameters)[:content]
            content.should_not be_empty
        end
    end

    context 'with invalid architecture' do
        let(:facts) {default_facts.merge({
            :architecture => 'x86_64_128_256_512_1024'
        })}

        it "dies" do
            expect {
                should contain_fail()
            }.to raise_error(Puppet::Error)
        end
    end
end
