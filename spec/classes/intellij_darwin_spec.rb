require 'spec_helper'

describe 'idea::intellij::darwin' do
    let(:title) { 'intellij_spec' }
    let(:facts) {
        {
			:charlie_repository => 'http://www.test-repository.example.com',
            :config_atlassian_home => '/home/horses/jesus',
            :config_user => 'bob',
            :config_group => 'bobgroup',
            :config_home => '/home/ponies',
            :config_charlie_home => '/home/charlie/directory',
        }
    }

    it "Sets up the IDEA package" do
        should contain_package('intellij_idea').with({
            'ensure'   => 'installed',
            'source'   => 'http://www.test-repository.example.com/ideaIU12_latest.dmg',
        })
    end

    it "Sets up IDEA settings" do
        should contain_exec('intellij_license').with({
			'command' => '/usr/bin/curl -L -s -o \'/home/ponies/Library/Preferences/IntelliJIdea12/idea12.key\' \'http://www.test-repository.example.com/idea12.key\'',
            'creates' => '/home/ponies/Library/Preferences/IntelliJIdea12/idea12.key',
        })
        should contain_file('intellij_license_ownership').with({
            'path'    =>
                '/home/ponies/Library/Preferences/IntelliJIdea12/idea12.key',
            'owner'   =>  'bob',
            'group'   =>  'bobgroup',
        })
        should contain_file('intellij_config').with({
            'ensure'  =>  'present',
            'path'    =>
                '/home/ponies/Library/Preferences/IntelliJIdea12/idea.vmoptions',
            'owner'   =>  'bob',
            'group'   =>  'bobgroup',
        })

        # Test that the intellij config file has content
        content = catalogue.resource('file', 'intellij_config').send(:parameters)[:content]
        content.should_not be_empty
    end
end
