require 'spec_helper'

describe 'jdk' do
    let(:title) { 'jvm_spec' }
    let(:facts) {
        {
            :charlie_repository => 'http://www.test-repository.example.com',
            :config_charlie_home => 'charlie_directory',
            :config_atlassian_home => '/some/target',
            :config_user => 'bob',
            :config_group => 'bobgroup', 
        }
    }

    context 'with operatingsystem => OSX' do
        let(:facts) {{:operatingsystem => 'Darwin'}}
        it {
            should include_class('jdk::darwin')
        }
    end

    context 'with operatingsystem => Ubuntu' do
        let(:facts) {{
            :operatingsystem        => 'Ubuntu',
            :operatingsystemrelease => '12.04',
        }}
        it {
            should include_class('jdk::ubuntu')
        }
    end

    context 'with operatingsystem => not_valid_OS' do
        let(:facts) {{:operatingsystem => 'BrickOS_4000'}}
        it {
            expect {
                should contain_fail()
            }.to raise_error(Puppet::Error)
        }
    end
end
